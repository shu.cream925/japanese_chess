import tkinter

object_width = 50       # オブジェクトの幅
object_height = 50      # オブジェクトの高さ
masume = 9              # マス目
masume_width = 100      # マス目の幅
masume_height = 100     # マス目の高さ
masume_x_start=100      # マス目の開始位置_X
masume_y_start=100      # マス目の開始位置_Y

def click(event):
    global figure
    global before_x, before_y

    x = canvas.canvasx(event.x)
    y = canvas.canvasy(event.y)

    # クリックされた位置に一番近い図形のID取得
    figure = canvas.find_closest(x, y)

    # マウスの座標を記憶
    before_x = x
    before_y = y

def drag(event):
    global before_x, before_y

    x = canvas.canvasx(event.x)
    y = canvas.canvasy(event.y)

    # 前回からのマウスの移動量の分だけ図形も移動
    canvas.move(
        figure,
        x - before_x, y - before_y
    )

    # マウスの座標を記憶
    before_x = x
    before_y = y

def drop(event):
    global before_x, before_y

    #座標取得
    x = canvas.canvasx(event.x)
    y = canvas.canvasy(event.y)

    # 一番近い座標を取得
    for tmp_x in range(masume) :
        if( (masume_x_start + masume_width * (tmp_x-1))< x )&( x < (masume_x_start + masume_width * tmp_x)):
            for tmp_y in range(masume) :
                if( (masume_y_start + masume_height * (tmp_y-1))< y )&( y < (masume_y_start + masume_height * tmp_y)):
                    near_x = masume_x_start + masume_width * (tmp_x-1) + object_width/2
                    near_y = masume_y_start + masume_height * (tmp_y-1) + object_height/2
                    break

    # デバッグ用
    # print( str(before_x) + " : " + str(before_y) + " : " + str(near_x) + " : " + str(near_y) )
    # print( str(x) + " : " + str(y) )
    # print( str(near_x - before_x) + " : " + str(near_y - before_y) )

    # 図形の移動
    canvas.moveto(
        figure,
        near_x, near_y  # 移動位置の指定
    )
    # 前回の座標を記憶
    before_x = near_x
    before_y = near_y

def main():
    global canvas

    app = tkinter.Tk()

    # キャンバス作成
    canvas = tkinter.Canvas(
        app,
        width=1000, height=1000,
        highlightthickness=0,
        bg="white",
        #scrollregion=(-1000, 0, 1000, 500),
    )
    canvas.grid(row=0, column=0)

    #画像ファイルを指定する
    img_file = tkinter.PhotoImage(file = R"..\img\stage\side_7.png")

    #画像ファイルをキャンバスの(0,0)に合わせて表示してみる
    canvas.create_image(500,500,image = img_file)

    #9x9のマス目を作る
      # 横線　(右の横軸,右の縦軸,左の横軸,左の縦軸)
    for i in range(11):
        canvas.create_line(masume_x_start+masume_width*masume, 
                           masume_x_start+masume_width*(i-1),  
                           masume_x_start,  
                           masume_x_start+masume_width*(i-1))                  # オプションなし


      # 縦線　(下の横軸,下の縦軸,上の横軸,上の縦軸)
    for i in range(11):
        canvas.create_line(masume_y_start+masume_height*(i-1),
                           masume_y_start+masume_height*masume,
                           masume_y_start+masume_height*(i-1),
                           masume_y_start)

    xbar = tkinter.Scrollbar(
    app,  # 親ウィジェット
    orient=tkinter.HORIZONTAL,  # バーの方向
)
    xbar.grid(
        row=1, column=0,  # キャンバスの下の位置を指定
        sticky=tkinter.W + tkinter.E  # 左右いっぱいに引き伸ばす
    )
    xbar.config(
        command=canvas.xview
    )
    canvas.config(
        xscrollcommand=xbar.set
    )

    # 色を用意
    colors = (
        "red", "green", "yellow", "blue", "purple", "pink", "orange"
    )

    # 色の数だけ適当に位置をずらしながら長方形（正方形）を描画
    for i, color in enumerate(colors):
        rect = canvas.create_rectangle(
            i * masume_width + masume_x_start + object_width/2,
            masume_y_start + object_height/2,
            i * masume_width + masume_x_start+masume_width  - object_width/2,
            masume_y_start + masume_height - object_height/2,
            fill=color,
        )

        # 描画した図形にイベント処理設定
        canvas.tag_bind(rect, "<ButtonPress-1>", click)
        canvas.tag_bind(rect, "<Button1-Motion>", drag)
        canvas.tag_bind(rect, "<ButtonRelease-1>", drop)

    app.mainloop()

if __name__ == "__main__":
    main()